+++
title = "Gitlab news 2019"
outputs = ["Reveal"]
+++

# Nouveautés de la forge Gitlab 2019

<a href="http://bit.ly/forge2019">http://bit.ly/forge2019</a>

Jacques Supcik, 8 Novembre 2019

<img src="img/isis.svg" style="width: 20rem; box-shadow: none;"/>

---

# gitlab.forge.hefr.ch

<img src="img/gitlab-logo-gray-rgb.png" style="width: 40rem; box-shadow: none;"/>


<div style="font-size: 6rem">
version "Ultimate" 🎆
</div>

---

# CI/CD

Construction, tests, déploiement en utilisant des "runners"

<a href="https://gitlab.forge.hefr.ch/jacques.supcik/hello-world-go" target="_blank" class="button">Hello World</a>

<img src="img/passed.png"/>


---

# Pages

Sites web statique.

<a href="https://gitlab.forge.hefr.ch/isis/presentation-forge-20191108" target="_blank" class="button">Self</a>

<a href="https://gitlab.forge.hefr.ch/programmierung/2019-2020/info" target="_blank" class="button">Cours de programmation (source)</a>

<a href="https://programmierung.pages.forge.hefr.ch/2019-2020/info/" target="_blank" class="button">Cours de programmation (site)</a>

---

# Docker

<img src="img/Moby-logo.png" style="box-shadow: none;"/>

---

# Registry

Dépot pour les images Docker

<a href="https://gitlab.forge.hefr.ch/jacques.supcik/hugo-builder" target="_blank" class="button">Hugo Builder</a>

<a href="https://gitlab.forge.hefr.ch/jacques.supcik/mkdocs-edu" target="_blank" class="button">Mkdocs Builder</a>

<a href="https://gitlab.forge.hefr.ch/embsys/docker-se12-builder" target="_blank" class="button">Arm Toolchain</a>

---

# Goldydocs

Pour vos projets plus ambitieux

<a href="https://example.docsy.dev/" target="_blank" class="button">Goldydoc</a>

---

# Templates

Pour démarrer rapidement vos projets

<img src="img/template.png"/>

---

# 😍 gitlab.forge.hefr.ch

<div style="font-size: 10rem">
Merci 🙏
</div>